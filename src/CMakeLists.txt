# add global dependencies
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/../labRemote/src/libCom )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/../labRemote/src/libPs )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/../labRemote/src/libUtils )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/../labRemote/src/libMeter )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/../labRemote/src/libLoad )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/../labRemote/src/libDevCom )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/../labRemote/src/libGalil )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/../labRemote/src/libZaber )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/../labRemote/src/libWaferProb )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/../labRemote/src/libScope )

link_directories( ${CMAKE_BINARY_DIR}/lib )

# add executables
SET(tools "StartDCS.cpp;CloseDCS.cpp;CheckDCS.cpp")

foreach(target ${tools})
  get_filename_component(execname ${target} NAME_WE)
  get_filename_component(srcfile ${target} NAME)

  add_executable(${execname} ${srcfile})
  target_link_libraries(${execname} -lCOM -lPS -lMETER -lLOAD -lDevCom)
  add_dependencies(${execname} COM PS METER LOAD DevCom)
endforeach()
