#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <math.h>

#include "RigolDP832.h"

#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include <thread> 



//Voltage and Current values that are planned to be used. 
const int NChan =3;
float VoltVal[NChan]={1.8,0,0};
float CurVal[NChan]={1.5,0,0};
float VoltTol = 0.2; // Max difference in observed voltage
float MaxCur = 0.7; //Maximum current allowed
bool ChanEnable[NChan]={true,false,false};





int closeDCS(std::string port) {

  //Initialize PS communication
  RigolDP832 PS(port);  

   
  std::cout<<"Status before disabeling"<<std::endl;


  std::cout<<"Disabelling All Channels"<<std::endl;
  for (int chan=1;chan<=NChan;chan++)
    {

      float Voltage = atof(PS.getVoltageStr(chan).c_str());
      float Current = atof(PS.getCurrentStr(chan).c_str());
      std::cout<<"Disabelling channel "<<chan<<" Voltage: "<<Voltage<<" Current: "<<Current<<std::endl;
      PS.turnOff(chan);
    }

  for (int chan=1;chan<=NChan;chan++)
    {
      float Voltage = atof(PS.getVoltageStr(chan).c_str());
      float Current = atof(PS.getCurrentStr(chan).c_str());

      std::cout<<"After Disable. Channel "<<chan<<" Voltage: "<<Voltage<<" Current: "<<Current<<std::endl;

      while ( Voltage>0 || Current >0)
	{
	  std:: cout<<"!!!!!!!!! Warninig DCS did not properly close!!!!!!!!!!!!!!!!!!!"<<std::endl;
	  std::cout<<"Trying Again"<<std::endl;
	  PS.turnOff(chan);
	  Voltage = atof(PS.getVoltageStr(chan).c_str());
	  Current = atof(PS.getCurrentStr(chan).c_str());

	}

    }

  std::cout<<"All Channels Voltage/Current is 0. All of them should be disabled"<<std::endl;

  return 0;
}



int startDCS(std::string port)
{


  //Initialize PS communication
  RigolDP832 PS(port);  


  std::cout<<"Setting the Voltage and Currents for:"<<std::endl;
  for (int chan=1;chan<=NChan;chan++)
    {
      if (ChanEnable[chan-1])
	std::cout<<" Chan "<<chan<<"; V: "<<VoltVal[chan-1]<<" A: "<<CurVal[chan-1]<<std::endl;
      else
	{
	  std::cout<<" Chan "<<chan<<"; Disabled"<<std::endl;
	  continue;
	}
      
      PS.setVoltageCurrent(chan, VoltVal[chan-1], CurVal[chan-1]);
      PS.turnOn(chan);
      std::this_thread::sleep_for (std::chrono::milliseconds(1000));
    }

  std::this_thread::sleep_for (std::chrono::milliseconds(500));
  
  std::cout<<std::endl<<std::endl<<std::endl<<"------------------"<<std::endl;
  std::cout<<"The measured voltages and currents accross channels are:"<<std::endl;
  for (int chan=1;chan<=NChan;chan++)
    {

      float Voltage = atof(PS.getVoltageStr(chan).c_str());
      float Current = atof(PS.getCurrentStr(chan).c_str());
      std::cout<<"Channel "<<chan<<" Voltage: "<<Voltage<<" Current: "<<Current<<std::endl;

      if ( fabs(VoltVal[chan-1]-Voltage)>0.2 || Current > MaxCur) 
	{
	  std::cout<<"Abnormal voltage observed, disabelling the DCS"<<std::endl;
	  closeDCS(port);
	  return -1;
	}



    }
  std::cout<<std::endl<<std::endl<<std::endl<<"------------------"<<std::endl;

  std::cout<<"Please check if the input voltage and currents are correct and type y <enter>. If not the DCS system will close in 5 seconds"<<std::endl;

  std::string n;

  fd_set readSet;
  FD_ZERO(&readSet);
  FD_SET(STDIN_FILENO, &readSet);
  struct timeval tv = {10, 0};  // 10 seconds, 0 microseconds;
  if (select(STDIN_FILENO+1, &readSet, NULL, NULL, &tv) < 0) perror("select");

  bool b = (FD_ISSET(STDIN_FILENO, &readSet)) ? bool(std::cin>>n) : false;

  if(n=="y" || n=="Y")
    std::cout<<"y is given, the DCS system will remain open. Please don't forget to turn it off later"<<std::endl;
  else
    {
      std::cout<<"No input or non y answer has been provided. The DCS system will shutdown."<<std::endl;  
      std::cout<<std::endl<<std::endl<<std::endl<<"------------------"<<std::endl;   
      closeDCS(port);
    }



  return 0;
}

int checkDCS(std::string port){

 RigolDP832 PS(port);  


 std::cout<<"Checking DCS Voltage"<<std::endl;

 while(true){
   std::cout<<std::endl<<std::endl<<std::endl<<"------------------"<<std::endl;
   std::cout<<"The measured voltages and currents accross channels are:"<<std::endl;
   for (int chan=1;chan<=NChan;chan++)
     {

       float Voltage = atof(PS.getVoltageStr(chan).c_str());
       float Current = atof(PS.getCurrentStr(chan).c_str());
       std::cout<<"Channel "<<chan<<" Voltage: "<<Voltage<<" Current: "<<Current<<std::endl;

       if ( (Voltage!=0 && fabs(VoltVal[chan-1]-Voltage)>0.2) || Current > MaxCur) 
	 {
	   std::cout<<"Abnormal voltage observed, disabelling the DCS"<<std::endl;
	   closeDCS(port);
	   return -1;
	 }



     }
   std::this_thread::sleep_for (std::chrono::seconds(3));   
   }
}
