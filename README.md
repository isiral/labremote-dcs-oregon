# labRemote-DCS-Oregon

#How to build
git submodule init
mkdir build
cd build
cmake3 ../
make
cd ../

#How to Run
cd build/bin/

#Start DCS system, After the initial start, user needs to type y and enter. Otherwise the DCS disables on it's own.
./StartDCS

#Close DCS system
./CloseDCS

#Monitor the Voltages
./CheckDSC

